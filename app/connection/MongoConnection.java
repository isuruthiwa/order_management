package connection;

import com.mongodb.MongoClient;

public class MongoConnection {
    private MongoClient mongoClient;
    private static MongoConnection mongoConnection;

    private MongoConnection(){
        mongoClient=new MongoClient("localhost");
    }

    public static MongoClient getInstance(){
        mongoConnection=mongoConnection==null?new MongoConnection():mongoConnection;
        return mongoConnection.mongoClient;
    }
}
