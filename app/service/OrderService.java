package service;

import com.fasterxml.jackson.databind.JsonNode;

public interface OrderService {
    boolean placeAnOrder(JsonNode jsonNode);
    boolean changeOrderStatus(int order_id, String orderStatus);
    JsonNode getOrders(int cust_id);
}
