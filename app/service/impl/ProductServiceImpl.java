package service.impl;

import bo.Product;
import com.fasterxml.jackson.databind.JsonNode;
import dao.ProductDao;
import play.libs.Json;
import service.ProductService;

public class ProductServiceImpl implements ProductService {
    private static ProductServiceImpl productService;
    private ProductDao productDao;

    private ProductServiceImpl(){
        productDao=ProductDao.getInstance();
    }

    public static ProductServiceImpl getInstance(){
        productService=productService==null?new ProductServiceImpl():productService;
        return productService;
    }

    @Override
    public void addProduct(JsonNode jsonNode) {
        productDao.addProduct(jsonNode.get("productName").asText(),jsonNode.get("stockQuantity").asInt(),jsonNode.get("pricePerUnit").asDouble());
    }

    @Override
    public boolean removeProduct(int product_id) {
        return productDao.removeProduct(product_id);
    }

    @Override
    public JsonNode getAllProducts() {
        return Json.toJson(productDao.getAllProducts());
    }

    @Override
    public JsonNode getProduct(int id) {
        Product product=productDao.getProduct(id);
        if(product!=null)
            return Json.toJson(product);
        else
            return null;
    }
}
