package service.impl;

import bo.Customer;
import com.fasterxml.jackson.databind.JsonNode;
import dao.CustomerDao;
import play.libs.Json;
import service.CustomerService;

import java.util.Iterator;

public class CustomerServiceImpl implements CustomerService {
    private static CustomerServiceImpl customerService;
    private CustomerDao customerDao;

    private CustomerServiceImpl(){
        customerDao=CustomerDao.getInstance();
    }

    public static CustomerServiceImpl getInstance(){
        customerService=customerService==null?new CustomerServiceImpl():customerService;
        return customerService;
    }


    @Override
    public void addCustomer(JsonNode jsonNode) {
        //Validation
        customerDao.addCustomer(jsonNode.get("name").asText(),jsonNode.get("address").get("addrline1").asText(),jsonNode.get("address").get("addrline2").asText(),
                jsonNode.get("address").get("streetNo").asInt(),jsonNode.get("address").get("landMark").asText(),jsonNode.get("address").get("city").asText(),jsonNode.get("address").get("country").asText(),
                jsonNode.get("address").get("zipcode").asInt(),jsonNode.get("age").asInt(),jsonNode.get("activeStatus").asBoolean());
    }

    @Override
    public boolean deleteCustomer(int cust_id) {
        return customerDao.removeCustomer(cust_id);
    }

    @Override
    public JsonNode getCustomer(int id) {
        Customer customer=customerDao.getCustomer(id);
        if(customer!=null) {
            return Json.toJson(customer);
        }else
            return null;
    }

    @Override
    public JsonNode getAllCustomers() {
        return Json.toJson(customerDao.getAllCustomers());
    }


}
