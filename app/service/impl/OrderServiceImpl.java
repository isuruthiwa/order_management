package service.impl;

import bo.Order;
import bo.OrderStatus;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import dao.CustomerDao;
import dao.OrderDao;
import dao.ProductDao;
import play.libs.Json;
import service.OrderService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class OrderServiceImpl implements OrderService {
    private static OrderServiceImpl orderService;
    private OrderDao orderDao;
    private CustomerDao customerDao;
    private ProductDao productDao;

    private OrderServiceImpl(){
        orderDao=OrderDao.getInstance();
        customerDao=CustomerDao.getInstance();
        productDao=ProductDao.getInstance();
    }

    public static OrderServiceImpl getInstance(){
        orderService=orderService==null?new OrderServiceImpl():orderService;
        return orderService;
    }

    @Override
    public boolean placeAnOrder(JsonNode jsonNode) {
        int cust_id=jsonNode.get("cust_id").asInt();
        if(customerDao.isExistingAndActiveCustomer(cust_id)) {
            HashMap<Integer, Integer> products = new HashMap<>();
            for (JsonNode node : jsonNode.get("products")) {
                int product_id=node.get("product_id").asInt();
                int quantity=node.get("quantity").asInt();
                if(!productDao.isStocksAvailable(product_id,quantity))
                    return false;
                products.put(product_id, quantity);
            }
            productDao.reduceAvailableStocks(products);
            return orderDao.addOrder(products, jsonNode.get("cust_id").asInt(),
                    jsonNode.get("date").asText(), jsonNode.get("orderStatus").asText());
        }
        else
            return false;
    }

    @Override
    public boolean changeOrderStatus(int order_id, String orderStatus) {
        return orderDao.updateOrderStatus(order_id, OrderStatus.valueOf(orderStatus));
    }

    @Override
    public JsonNode getOrders(int cust_id) {
        ArrayList<Order> orderArrayList=orderDao.getOrders(cust_id);
        if(orderArrayList.isEmpty())
            return null;
        else
            return Json.toJson(orderArrayList);
    }


}
