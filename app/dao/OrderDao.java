package dao;

import bo.Order;
import bo.OrderStatus;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;
import connection.MongoConnection;
import org.bson.Document;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class OrderDao {
    private static OrderDao orderDao;
    MongoClient mongoClient;
    ArrayList<Order> orders;
    MongoCollection<Document> orderCollection;

    private OrderDao(){
        orders=new ArrayList<>();
        mongoClient= MongoConnection.getInstance();
        orderCollection=mongoClient.getDatabase("order_management").getCollection("orders");
        readOrderCollection();
    }

    public static OrderDao getInstance(){
        orderDao=orderDao==null?new OrderDao():orderDao;
        return orderDao;
    }

    void readOrderCollection(){
        for (Document document : orderCollection.find()) {
            int order_id=-1;
            HashMap<Integer,Integer> products=null;
            int cust_id=-1;
            String date=null;
            OrderStatus orderStatus=null;

            for (Map.Entry<String, Object> set : document.entrySet()) {
                if (set.getKey().equals("order_id")) {
                    order_id = (int) set.getValue();
                }
                else if (set.getKey().equals("products")){
                    products=new HashMap<>();
                    ArrayList<Document> productList= (ArrayList<Document>) set.getValue();
                    for (Object productDetails : productList) {
                        Document productDocument =(Document)productDetails;
                        int product_id= productDocument.getInteger("product_id");
                        int quantity= productDocument.getInteger("quantity");
                        products.put(product_id,quantity);
                    }
                }
                else if (set.getKey().equals("cust_id"))
                    cust_id = (int) set.getValue();
                else if (set.getKey().equals("date"))
                    date = (String) set.getValue();
                else if (set.getKey().equals("orderStatus")) {
                    orderStatus = OrderStatus.valueOf(set.getValue().toString());
                }
            }
            if(order_id!=-1 && products!=null && cust_id!=-1 && date!=null && orderStatus!=null) {
                orders.add(new Order(order_id, products, cust_id, date, orderStatus));
            }
        }

    }

    public boolean addOrder(HashMap<Integer,Integer> products, int cust_id, String date, String orderStatus){
        Order order=new Order((int) (orderCollection.count()+1),products,cust_id,date,OrderStatus.valueOf(orderStatus));
        Document orderDocument=new Document();
        orderDocument.put("order_id", (int) (orderCollection.count() + 1));
        ArrayList<Document> productOrders = new ArrayList<>();
        for(Map.Entry<Integer,Integer> entry: order.getProducts().entrySet()){
            Document orderEntry = new Document();
            orderEntry.put("product_id",entry.getKey());
            orderEntry.put("quantity",entry.getValue());
            productOrders.add(orderEntry);
        }
        orderDocument.put("products",productOrders);
        orderDocument.put("cust_id",cust_id);
        orderDocument.put("date",order.getDate());
        orderDocument.put("orderStatus",order.getOrderStatus().toString());
        orderCollection.insertOne(orderDocument);
        orders.add(order);

        return true;
    }



    public ArrayList<Order> getOrders(int cust_id){
        ArrayList<Order> ordersByCust = new ArrayList<>();
        for (Order order: orders){
            if(order.getCust_id()==cust_id){
                ordersByCust.add(order);
            }
        }
        return ordersByCust;
    }

    public boolean updateOrderStatus(int order_id,OrderStatus orderStatus){
        for(Order order: orders){
            if(order.getOrder_id()==order_id){
                if(order.getOrderStatus().equals(OrderStatus.PENDING)){
                    if(orderStatus.equals(OrderStatus.IN_PROGRESS) || orderStatus.equals(OrderStatus.CANCELLED)) {
                        order.updateOrderStatus(orderStatus);
                        orderCollection.updateOne(Filters.eq("order_id",order_id), Updates.set("orderStatus",orderStatus.toString()));
                        return true;
                    }
                    else
                        return false;
                }
                else if(order.getOrderStatus().equals(OrderStatus.IN_PROGRESS)){
                    if(orderStatus.equals(OrderStatus.DELIVERED) || orderStatus.equals(OrderStatus.CANCELLED)) {
                        order.updateOrderStatus(orderStatus);
                        orderCollection.updateOne(Filters.eq("order_id",order_id), Updates.set("orderStatus",orderStatus.toString()));
                        return true;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
        }
        return false;
    }
}
