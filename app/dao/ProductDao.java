package dao;

import bo.Customer;
import bo.Product;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;
import connection.MongoConnection;
import org.bson.Document;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ProductDao {
    private static ProductDao productDao;
    MongoClient mongoClient;
    ArrayList<Product> products;
    MongoCollection<Document> productCollection;

    private ProductDao(){
        products=new ArrayList<>();
        mongoClient= MongoConnection.getInstance();
        productCollection=mongoClient.getDatabase("order_management").getCollection("products");
        readProductCollection();
    }

    public static ProductDao getInstance(){
        productDao=productDao==null?new ProductDao():productDao;
        return productDao;
    }

    void readProductCollection(){
        for (Document document : productCollection.find()) {
            int product_id=-1;
            String productName=null;
            int stockQuantity=-1;
            double pricePerUnit=-1;

            for (Map.Entry<String, Object> set : document.entrySet()) {
                if (set.getKey().equals("product_id")) {
                    product_id = (int) set.getValue();
                }
                else if (set.getKey().equals("productName"))
                    productName = (String) set.getValue();
                else if (set.getKey().equals("stockQuantity"))
                    stockQuantity = (int) set.getValue();
                else if (set.getKey().equals("pricePerUnit"))
                    pricePerUnit = (double) set.getValue();
            }
            if(product_id!=-1 && productName!=null && stockQuantity!=-1 && pricePerUnit!=-1) {
                products.add(new Product(product_id, productName, stockQuantity, pricePerUnit));
            }
        }
    }

    public void addProduct(String productName, int stockQuantity, double pricePerUnit){
        Product product=new Product((int) (productCollection.count()+1),productName,stockQuantity,pricePerUnit);
        Document productDocument=new Document();
        productDocument.put("product_id", (int) (productCollection.count() + 1));
        productDocument.put("productName",product.getProductName());
        productDocument.put("stockQuantity",product.getStockQuantity());
        productDocument.put("pricePerUnit",product.getPricePerUnit());
        productCollection.insertOne(productDocument);
        products.add(product);
    }

    public Product getProduct(int product_id){
        for(Product product:products){
            if(product.getProduct_id()==product_id){
                return product;
            }
        }
        return null;
    }

    public ArrayList<Product> getAllProducts(){
        return products;
    }

    public boolean removeProduct(int product_id){
        productCollection.deleteOne(Filters.eq("product_id",product_id));
        return products.remove(getProduct(product_id));
    }

    public boolean isStocksAvailable(int product_id,int quantity){
        for(Product product: products){
            if(product.getProduct_id()==product_id) {
                return (product.getStockQuantity() > quantity);
            }
        }
        return false;
    }

    public void reduceAvailableStocks(HashMap<Integer, Integer> productOrders) {
        for(Product product: products){
            for(Map.Entry<Integer,Integer> productOrder: productOrders.entrySet()){
                if(product.getProduct_id()==productOrder.getKey()){
                    product.reduceAvailableStock(productOrder.getValue());
                    productCollection.updateOne(Filters.eq("product_id",productOrder.getKey()),
                            Updates.inc("stockQuantity",productOrder.getValue()*-1));
                }
            }
        }
    }
}
