package bo;

import java.util.HashMap;

public class Order {
    private int order_id;
    private HashMap<Integer,Integer> products;
    private int cust_id;
    private String date;
    private OrderStatus orderStatus;

    public Order(int order_id, HashMap<Integer, Integer> products, int cust_id, String date, OrderStatus orderStatus) {
        this.order_id = order_id;
        this.products = products;
        this.cust_id = cust_id;
        this.date = date;
        this.orderStatus = orderStatus;
    }

    public int getOrder_id() {
        return order_id;
    }

    public HashMap<Integer, Integer> getProducts() {
        return products;
    }

    public int getCust_id() {
        return cust_id;
    }

    public String getDate() {
        return date;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    @Override
    public String toString() {
        return "Order{" +
                "order_id=" + order_id +
                ", products=" + products +
                ", cust_id=" + cust_id +
                ", date='" + date + '\'' +
                ", orderStatus=" + orderStatus +
                '}';
    }

    public void updateOrderStatus(OrderStatus orderStatus){
        this.orderStatus=orderStatus;
    }
}
