package bo;

public class Customer {
    private int cust_id;
    private String name;
    private String addrline1;
    private String addrline2;
    private int streetNo;
    private String landMark;
    private String city;
    private String country;
    private int zipcode;
    private int age;
    private boolean activeStatus;


    public Customer(int cust_id, String name, String addrline1, String addrline2, int streetNo, String landMark, String city, String country, int zipcode, int age, boolean activeStatus) {
        this.cust_id = cust_id;
        this.name = name;
        this.addrline1 = addrline1;
        this.addrline2 = addrline2;
        this.streetNo = streetNo;
        this.landMark = landMark;
        this.city = city;
        this.country = country;
        this.zipcode = zipcode;
        this.age = age;
        this.activeStatus = activeStatus;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "cust_id=" + cust_id +
                ", name='" + name + '\'' +
                ", addrline1='" + addrline1 + '\'' +
                ", addrline2='" + addrline2 + '\'' +
                ", streetNo=" + streetNo +
                ", landMark='" + landMark + '\'' +
                ", city='" + city + '\'' +
                ", country='" + country + '\'' +
                ", zipcode=" + zipcode +
                ", age=" + age +
                ", activeStatus=" + activeStatus +
                '}';
    }

    public int getCust_id() {
        return cust_id;
    }

    public String getName() {
        return name;
    }

    public String getAddrline1() {
        return addrline1;
    }

    public String getAddrline2() {
        return addrline2;
    }

    public int getStreetNo() {
        return streetNo;
    }

    public String getLandMark() {
        return landMark;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }

    public int getZipcode() {
        return zipcode;
    }

    public int getAge() {
        return age;
    }

    public boolean isActiveStatus() {
        return activeStatus;
    }
}
