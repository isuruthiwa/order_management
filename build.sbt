name := """OrderManagement"""
organization := "com.nCinga"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.13.1"

libraryDependencies += guice
libraryDependencies += "org.pac4j" % "pac4j-mongo" % "3.8.3"
libraryDependencies += "com.typesafe.play" %% "play-json" % "2.8.1"